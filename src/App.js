import "./App.css";
import Dashboard from "./components/dashboard";
import "antd/dist/antd.css";

function App() {
  return (
    <div className="App">
      <Dashboard />
    </div>
  );
}

export default App;
